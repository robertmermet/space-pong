# Space Pong

A game inspired by Pong and Space Invaders.

Uses PressStart2P font type by CodeMan38.

Uses sound effects by NoiseCollector, n_audioman, MusicLegends, Soundholder, resofactor, TolerableDruid6 and OwlStorm.

#### Clone Repo

    git clone git@gitlab.com:robertmermet/space-pong.git

#### View Demo

>**demo** [robertmermet.com/projects/space-pong](http://robertmermet.com/projects/space-pong)